﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora_Pokemon.Views
{
    public partial class Form4 : MetroFramework.Forms.MetroForm
    {
        int temp = 0;
        public Form4()
        {
            InitializeComponent();
        }

        
        private void Form4_Load(object sender, EventArgs e)
        {
            //as labels so servem para guardar os resultados
            label_num1.Hide();
            label_num2.Hide();
            label_op.Hide();
            label_resultado.Hide();
               

            //sabendo os numeros que fazem a conta escolhido pelo utilizador na form3, conseguimos saber 
            //o numero de imagens das pokeballs (picturesbox) que temos de fazer aparecer no ecra
            
            //carregar a imagem pela diretoria
            string[] s = { "\\bin" };
            string path = Application.StartupPath.Split(s, StringSplitOptions.None)[0] + "\\Imagens\\Pokebola2.png";
            Image pokeball = Image.FromFile(path);
            

            // cria lista de imagens pela class ImageList ( class Image nao adiciona imagens -.-)
            var ListaImagem = new ImageList();

            //adicionar a imagem +a lista de imagens criada
            ListaImagem.Images.Add("PokeballKey", pokeball);

            // dizer á listview para usar a imagem
            listView1.LargeImageList = ListaImagem;            
            listView3.LargeImageList = ListaImagem;
            listView4.LargeImageList = ListaImagem;



            
            do
            {
                //adiciona uma pokeball na listview 
                var listViewItem1 = listView1.Items.Add("");
               
                listViewItem1.ImageKey = "PokeballKey";                
                temp++;
                
            } while (temp != Convert.ToInt32(label_num1.Text));

            

            temp = 0;

            if(label_op.Text == "+")
            {
                
                listView2.Items.Add("+");

         metroLabel3.Text = "Se tiveres " + label_num1.Text.ToString()
          + " pokebolas e te derem mais " + label_num2.Text.ToString() + Environment.NewLine +
        " ficas com " + label_resultado.Text.ToString() + " pokebolas";


            }
            else if (label_op.Text == "-")
            {
                listView2.Items.Add("-");

                metroLabel3.Text = "Se tiveres " + label_num1.Text.ToString()
                 + " pokebolas mas perderes " + label_num2.Text.ToString() + Environment.NewLine +
               " ficas com " + label_resultado.Text.ToString() + " pokebolas";
               
            }
            

            do
            {
                //adiciona uma pokeball na listview 
                var listViewItem3 = listView3.Items.Add("");
               
                listViewItem3.ImageKey = "PokeballKey";
                temp++;
            } while (temp != Convert.ToInt32(label_num2.Text));

            temp = 0;
            listView5.Items.Add("=");

            if(label_resultado.Text!="0")
            {
            do
            {
                //adiciona uma pokeball na listview 
                var listViewItem4 = listView4.Items.Add("");
                listViewItem4.ImageKey = "PokeballKey";
                temp++;
            } while (temp != Convert.ToInt32(label_resultado.Text));

            }
            else {
                listView4.Items.Add("0");
            }

        }

        public string _num1
        {
            set { label_num1.Text = value; }
        }

        public string _op
        {
            set { label_op.Text = value; }
        }

        public string _num2
        {
            set { label_num2.Text = value; }
        }

        public string _resultado
        {
            set { label_resultado.Text = value; }
        }

        private void metroLink1_Click(object sender, EventArgs e)
        {
            Form3 form = new Form3();
            this.Hide();
            form.Show();
        }

        private void ml_mais_Click(object sender, EventArgs e)
        {

        }

        private void metroLabel3_Click(object sender, EventArgs e)
        {

        }

        private void label_num1_Click(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listView3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listView5_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
