﻿namespace Calculadora_Pokemon.Views
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.metroButton5 = new MetroFramework.Controls.MetroButton();
            this.metroLink1 = new MetroFramework.Controls.MetroLink();
            this.pb_ball = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_num1 = new System.Windows.Forms.TextBox();
            this.tb_resultado = new System.Windows.Forms.TextBox();
            this.tb_num2 = new System.Windows.Forms.TextBox();
            this.tb_op = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ball)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.metroLabel1.Location = new System.Drawing.Point(17, 73);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(392, 68);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.White;
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroLabel1.Click += new System.EventHandler(this.metroLabel1_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(330, 157);
            this.metroButton1.Margin = new System.Windows.Forms.Padding(2);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(56, 63);
            this.metroButton1.TabIndex = 2;
            this.metroButton1.Text = "+";
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroButton3
            // 
            this.metroButton3.Location = new System.Drawing.Point(330, 264);
            this.metroButton3.Margin = new System.Windows.Forms.Padding(2);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(56, 63);
            this.metroButton3.TabIndex = 4;
            this.metroButton3.Text = "-";
            this.metroButton3.Click += new System.EventHandler(this.metroButton3_Click);
            // 
            // metroButton5
            // 
            this.metroButton5.Location = new System.Drawing.Point(330, 369);
            this.metroButton5.Margin = new System.Windows.Forms.Padding(2);
            this.metroButton5.Name = "metroButton5";
            this.metroButton5.Size = new System.Drawing.Size(56, 90);
            this.metroButton5.TabIndex = 6;
            this.metroButton5.Text = "=";
            this.metroButton5.Click += new System.EventHandler(this.metroButton5_Click);
            // 
            // metroLink1
            // 
            this.metroLink1.Location = new System.Drawing.Point(58, 416);
            this.metroLink1.Margin = new System.Windows.Forms.Padding(2);
            this.metroLink1.Name = "metroLink1";
            this.metroLink1.Size = new System.Drawing.Size(177, 58);
            this.metroLink1.TabIndex = 7;
            this.metroLink1.Text = "Explicar Resultado";
            this.metroLink1.Click += new System.EventHandler(this.metroLink1_Click);
            // 
            // pb_ball
            // 
            this.pb_ball.Image = ((System.Drawing.Image)(resources.GetObject("pb_ball.Image")));
            this.pb_ball.Location = new System.Drawing.Point(44, 157);
            this.pb_ball.Margin = new System.Windows.Forms.Padding(2);
            this.pb_ball.Name = "pb_ball";
            this.pb_ball.Size = new System.Drawing.Size(224, 239);
            this.pb_ball.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_ball.TabIndex = 0;
            this.pb_ball.TabStop = false;
            this.pb_ball.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(230, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 33);
            this.label1.TabIndex = 8;
            this.label1.Text = "=";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tb_num1
            // 
            this.tb_num1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_num1.Location = new System.Drawing.Point(33, 89);
            this.tb_num1.Multiline = true;
            this.tb_num1.Name = "tb_num1";
            this.tb_num1.Size = new System.Drawing.Size(45, 33);
            this.tb_num1.TabIndex = 9;
            this.tb_num1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_resultado
            // 
            this.tb_resultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_resultado.Location = new System.Drawing.Point(277, 89);
            this.tb_resultado.Multiline = true;
            this.tb_resultado.Name = "tb_resultado";
            this.tb_resultado.Size = new System.Drawing.Size(45, 33);
            this.tb_resultado.TabIndex = 10;
            this.tb_resultado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tb_num2
            // 
            this.tb_num2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_num2.Location = new System.Drawing.Point(174, 89);
            this.tb_num2.Multiline = true;
            this.tb_num2.Name = "tb_num2";
            this.tb_num2.Size = new System.Drawing.Size(45, 33);
            this.tb_num2.TabIndex = 11;
            this.tb_num2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tb_num2.TextChanged += new System.EventHandler(this.tb_num2_TextChanged);
            // 
            // tb_op
            // 
            this.tb_op.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_op.Location = new System.Drawing.Point(103, 89);
            this.tb_op.Multiline = true;
            this.tb_op.Name = "tb_op";
            this.tb_op.Size = new System.Drawing.Size(45, 33);
            this.tb_op.TabIndex = 12;
            this.tb_op.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 484);
            this.Controls.Add(this.tb_op);
            this.Controls.Add(this.tb_num2);
            this.Controls.Add(this.tb_resultado);
            this.Controls.Add(this.tb_num1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.metroLink1);
            this.Controls.Add(this.metroButton5);
            this.Controls.Add(this.metroButton3);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.pb_ball);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form3";
            this.Padding = new System.Windows.Forms.Padding(15, 60, 15, 16);
            this.Text = "Calculadora";
            ((System.ComponentModel.ISupportInitialize)(this.pb_ball)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb_ball;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroButton metroButton5;
        private MetroFramework.Controls.MetroLink metroLink1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_num1;
        private System.Windows.Forms.TextBox tb_resultado;
        private System.Windows.Forms.TextBox tb_num2;
        private System.Windows.Forms.TextBox tb_op;
    }
}