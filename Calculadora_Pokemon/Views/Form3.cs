﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora_Pokemon.Views
{
    public partial class Form3 : MetroFramework.Forms.MetroForm
    {
        int num1 = 1; //valor da tb_num1
        int num2 = 1;  //valor da tb_num2
        int resultado; //valor do resultado tb_resultado
        bool valornegativo = false;  //saber se o resultado da negativo

        public Form3()
        {
            InitializeComponent();

        }

        //private void Form3_Load(object sender, EventArgs e)
        //{
        //   MessageBox.Show("Clica com o rato na pokebola para começares a fazer contas");

        //}

        //evento ao cliar na pokeball
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            // se clicar na pokeball apos uma conta ja ter sido feita faz reset para comecar nova conta
            if(tb_resultado.Text.Length != 0)
            {
                tb_num1.Clear();
                tb_num2.Clear();
                tb_op.Clear();
                tb_resultado.Clear();
                num1 = 1;
                num2 = 1;
                resultado = 0;
            }

            //se ainda  nao for escolhido o sinal da operaçao, so adicona no primeiro numero
            if (tb_op.Text.Length == 0) 
            {
                if(tb_num1.Text.ToString() == "10") 
                {
                    MessageBox.Show("So numeros de 0 a 10");
                    tb_num1.Clear();
                    num1 = 1;
                }
                else //senao ultrapassou o valor 10 incrementa 
                {
                    tb_num1.Text = Convert.ToInt32(num1++).ToString();
                }
            }
            //se ja escolheu o sinal da operaçao, adicona no segundo numero
            if (tb_op.Text.Length != 0)
            {

            if (tb_num2.Text.ToString() == "10")
            {
                MessageBox.Show("So numeros de 0 a 10");
                tb_num2.Clear();
                num2 = 1;
            }
            else
            {               
                    if(tb_op.Text=="-")
                    {                    
                    //verificar se o resultado pode dar negativo-so acontece no sinal "-"
                    if(num2<num1)
                    {
                            valornegativo = false;
                        tb_num2.Text = Convert.ToInt32(num2++).ToString();
                    }
                    else
                        {//se num2>mun1 da resultado negativo, nao escreve num2
                            valornegativo = true;
                            MessageBox.Show("Numa subtração,se o segundo numero for maior que o primeiro a conta da valor negativo");
                            tb_num2.Clear();
                        num2 = 1;
                    } 
                    }
                    else //se o sinal for "+" nao precisamos de verificar se da negativo,
                    //incrementa logo
                    {
                        valornegativo = false;
                        tb_num2.Text = Convert.ToInt32(num2++).ToString();
                    }
                }
            }
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            if (tb_num1.Text.Length != 0) //nao deixa clicar numa op sem ter escolhido o 1º numero e se o num2 for maior
            {                 
                    tb_op.Text = "-";
                       
            }
            else { }
        }

        private void metroLabel1_Click(object sender, EventArgs e)
        {

        }

        public string _num1
        {
            get { return tb_num1.Text; }
        }

        public string _op
        {
            get { return tb_op.Text; }
        }



        public string _num2
        {
            get { return tb_num2.Text; }
        }

        public string _resultado
        {
            get { return tb_resultado.Text; }
        }
        
        //explicar resultado
        private void metroLink1_Click(object sender, EventArgs e)
        {
            if(resultado<=10 && resultado >=0) //se o resultado for >10 ou <0 faz reset
            {         
            // se a conta ainda nao estiver feita nao aceita ir para form explicar resultado
            if(tb_num1.Text.Length != 0 && tb_num2.Text.Length !=0 && tb_resultado.Text.Length != 0)
            {
                Form4 form = new Form4();
                //cada numero das tb_num1, tb_num2 , tb_resultado vai ser instanciado no form4 
                form._num1 = _num1;
                form._num2 = _num2;
                form._resultado = _resultado;
                form._op = _op;
                this.Hide();
                form.Show();
            }
            else
            {
                MessageBox.Show("Tens de fazer um conta primeiro para saberes a explicação do resultado");
            }
            }else {
                MessageBox.Show("Ainda so aprendeste a calcular numeros de 0 a 10 :)");
                tb_num1.Clear();
                tb_num2.Clear();
                tb_op.Clear();
                tb_resultado.Clear();
                num1 = 1;
                num2 = 1;
                resultado = 0;
            }

        }

        private void metroButton4_Click(object sender, EventArgs e)
        {

        }

        private void metroButton2_Click(object sender, EventArgs e)
        {

        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            if(tb_num1.Text.Length != 0) //nao deixa clicar numa op sem ter escolhido o 1º numero
            {                              
                tb_op.Text = "+";
            }
            else { }
               

        }

        private void metroButton5_Click(object sender, EventArgs e)
        {
            if (valornegativo == false && tb_num1.Text.Length != 0 && tb_num2.Text.Length != 0 && tb_op.Text.Length != 0)
            {
                switch (tb_op.Text)
                {
                    case "+":
                        resultado = (num1 - 1) + (num2 - 1);
                        tb_resultado.Text = resultado.ToString();

                        break;

                    case "-":
                        resultado = (num1 - 1) - (num2 - 1);
                        tb_resultado.Text = resultado.ToString();
                        break;
                    default: tb_resultado.Text = "0";
                        break;

                    
                }
                if(resultado<0)
                {
                    MessageBox.Show("Numa subtração,se o segundo numero for maior que o primeiro a conta da valor negativo");

                }

            }
            else { }
        }

        private void tb_num2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
